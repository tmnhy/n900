# README #


### What is it? ###

Root JFFS2 image for Nokia N900 (RX-51) tablet, based on official firmware with some changes.


### Whom is it for? ###

For advanced users who can work with flasher.


### For what? ###

For quick installation and upgrade to actual Community SSU after full hard reset.


### Features ###

* based on official firmware PR 1.3 version 20.2010.36-2 (RX-51_2009SE_20.2010.36-2_PR_COMBINED_MR0_ARM.bin);
* out-of-the-box openssh-server;
* out-of-the-box root access (over ssh);
* updated application catalogues;
* active catalogue:
    - Maemo Extras, Extras Devel, Extras Testing;
    - Maemo SDK;
    - Maemo Tool;
    - Comminity SSU
* present but not active:
    - Comminity SSU (testing)
    - Comminity SSU (devel)
    - Comminity SSU (thumb)


### DISCLAIMER ###

Flashing a firmware always contains the risk of breaking your device, making it unusable and loses user data.
Please follow this guide at your own risk and backing up your data.


### Installation, typical use ###

* get rootfs JFFS2 image from this repository https://bitbucket.org/tmnhy/n900/downloads/rootfs.jffs2
* optional, flash latest vanilla version of the eMMC content and Maemo 5 firmware;
* flash rootfs image

```
flasher-3.5 -f -R -r rootfs.jffs2
```

* after device has successfully booted and network activated you have root access (by default root passsword is **_toor_**, change it necessarily)
from X Terminal

```
$ ssh root@localhost
```

or remote

```
$ ssh root@ip_your_n900_device
```

* update the system

```
# apt-get update && apt-get install mp-fremantle-community-pr
```

* Enjoy, you have a version 21.2011.38-1

* What's next? Enable in application manager additional community repository (testing, devel, thumb) and upgrade your device to latest 21.2011.38-1Tmaemo11+thumb0.